package org.springframework.gs_test_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */

@SpringBootApplication(scanBasePackages = {"org.springframework.controllers"})
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        SpringApplication.run(App.class, args);
    }
}
 